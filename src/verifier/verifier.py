from crypt import methods
import json
import os
from flask import Blueprint, jsonify, render_template, request
from flask_login import login_required

verifier = Blueprint('verifier', __name__, template_folder='templates')

SERVICE = 'apache2.service'


@verifier.route('/dashboard')
@login_required
def index():
    return render_template('dashboard.html', root_url=request.url_root[:-1])


@verifier.route('/fetch_logs', methods=['GET'])
@login_required
def fetch_logs():
    logs = [
        {'time_stamp': '2022-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:31:00.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-08-09 20:30:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:20.945', 'verify': 'VERIFY_KO', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2021-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:20.945', 'verify': 'VERIFY_KO', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:10.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-03-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:20:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-04-09 20:00:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:11:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 19:30:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 20:30:16.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']},
        {'time_stamp': '2022-05-09 18:20:20.945', 'verify': 'VERIFY_OK', 'attributes': ['Cheat', 'Cert', 'Attribute 11', 'Attribute 13']}
    ]

    return jsonify(logs)


@verifier.route('/service_status', methods=['GET'])
@login_required
def service_status():
    stat = os.system(f'systemctl is-active {SERVICE}')
    if stat == 0:
        return jsonify({'active': True})
    else:
        return jsonify({'active': False})


def service_action(action):
    status = False
    stat = os.system(f'systemctl {action} {SERVICE}')
    if stat == 0:
        status = True
    return status


@verifier.route('/service_control', methods=['POST'])
@login_required
def service_control():
    data = request.get_json()
    if data['action'] == 'On':
        status = service_action('start')
        start = True
    else:
        status = service_action('stop')
        start = False
    return jsonify({'status': status, 'start': start})
        

@verifier.route('/fetch_attributes', methods=['GET'])
@login_required
def fetch_attributes():
    try:
        with open('./src/verifier/setup/attrs.json', 'r') as fp:
            attr = json.load(fp)
        return jsonify({'data': attr})
    except IOError:
        return jsonify({'data': []})


@verifier.route('/save_attributes', methods=['POST'])
@login_required
def save_attributes():
    data = request.get_json()
    if data:
        with open('./src/verifier/setup/attrs.json', 'w') as fp:
            json.dump(data, fp, indent=4)
        return jsonify({'status': True})
    else:
        return jsonify({'status': False})
