import os
from flask import Flask
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from .config import app_config


db =  SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()


def create_app():
    app = Flask(__name__)
    app.config.from_mapping(**app_config())
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)

    with app.app_context():
        from .auth.auth import auth
        from .verifier.verifier import verifier

        app.register_blueprint(auth)
        app.register_blueprint(verifier)
        return app, db