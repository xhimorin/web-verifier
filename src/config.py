import os.path
from os.path import exists
from yaml import safe_load


def _load_config(file_name='/../config.yaml'):
    file = os.path.dirname(__file__) + file_name
    if exists(file):
        with open(file, 'r') as fr:
            config = safe_load(fr)
        return config
    else:
        print(f'{file_name} doesnt exists!')
        return {}


def app_config():
    try:
        return _load_config()['APP']
    except KeyError:
        print('[!!] Missing field in config file!')
        return {}



def google_config():
    try:
        return _load_config()['GOOGLE_API']
    except KeyError:
        print('[!!] Missing field in config file!')
        return {}
