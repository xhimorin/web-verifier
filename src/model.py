from flask_login import UserMixin

from . import db


class User(UserMixin, db.Model):

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.String(32), nullable=False, unique=True)

    def __init__(self, account_id) -> None:
        self.account_id = account_id

    def save(self):
        db.session.add(self)
        db.session.commit()

    def is_authenticated(self):
        return super().is_authenticated

    @staticmethod
    def user_by_account_id(account_id):
        return User.query.filter(User.account_id == account_id).first()

    @staticmethod
    def person_by_id(id):
        return User.query.filter(User.id == id).first()
