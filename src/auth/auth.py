from flask import Blueprint, render_template, request, redirect, url_for, jsonify
from flask_login import (
    current_user, login_manager, login_required, logout_user, login_user
)
import requests, json
from oauthlib.oauth2 import WebApplicationClient

from src.config import google_config
from src import login_manager
from src.model import User


CONFIG = google_config()
CLIENT_ID = CONFIG['GOOGLE_CLIENT_ID']
CLIENT_SECRET = CONFIG['GOOGLE_CLIENT_SECRET']
DISCOVERY_URL = CONFIG['GOOGLE_DISCOVERY_URL']

auth = Blueprint(
    'auth',
    __name__,
    template_folder='templates'
)
client = WebApplicationClient(CLIENT_ID)
login_manager.login_view = "auth.login"


def get_google_provider_cfg():
    return requests.get(DISCOVERY_URL).json()


@auth.route('/')
def index():
    return redirect(url_for('auth.login'))


@auth.route('/login', methods=['GET'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('verifier.index'))
    else:
        return render_template('login.html', root_url=request.url_root[:-1])


@auth.route('/authorization')
def authorization():
    google_provider_cfg = get_google_provider_cfg()
    authorization_endpoint = google_provider_cfg['authorization_endpoint']

    request_uri = client.prepare_request_uri(
        authorization_endpoint,
        redirect_uri=request.base_url + '/callback',
        scope=['openid', 'email', 'profile']
    )

    return redirect(request_uri)


@auth.route('/authorization/callback')
def authorization_callback():
    code = request.args.get('code')

    google_provider_cfg = get_google_provider_cfg()
    token_endpoint = google_provider_cfg['token_endpoint']

    token_url, header, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=request.url,
        redirect_url=request.base_url,
        code=code
    )
    token_response = requests.post(
        token_url,
        headers=header,
        data=body,
        auth=(CLIENT_ID, CLIENT_SECRET)
    )
    
    client.parse_request_body_response(json.dumps(token_response.json()))

    userinfo_endpoint = google_provider_cfg['userinfo_endpoint']
    uri, header, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=header, data=body)

    if userinfo_response.json().get('email_verified'):
        account_id =  userinfo_response.json()['sub']
    else:
        return jsonify({'status': False, 'authorized': False}), 401

    user = User.user_by_account_id(account_id)

    if user is None:
        user = User(account_id)
        user.save()
    login_user(user, remember=True)
    return redirect(url_for('auth.login'))


@auth.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))


@login_manager.user_loader
def load_user(id):
    if id is not None:
        return User.person_by_id(id)
    return None


@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect(url_for('auth.login'))

